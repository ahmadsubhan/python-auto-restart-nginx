# Auto restart python script
# Ahmad @ Oct 16, 2018
# 
# Usage: python auto-restart-nginx.py "check"
#        python auto-restart-nginx.py "restart"

from subprocess import check_output, call, STDOUT, CalledProcessError
import argparse

def get_status():
    status = 'unknown'

    try:
        output = check_output(['service', 'nginx', 'status'], stderr=STDOUT)
    except CalledProcessError as ex:
        output = ex.output

    # Statua active (started)
    if 'Active: active (running)' in output:
        status = 'active'

    # Status inactive (stopped)
    elif 'Active: inactive (dead)' in output:
        status = 'inactive'

    return status

def force_restart():
    print("Restarting...")

    try:
        output = call(['service', 'nginx', 'restart'], stderr=STDOUT)
        print("Restarted")
    except CalledProcessError as ex:
        output = ex.output
        print("Error: " + output)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('action', help='Action if not running, possible values: "check", "restart"')
    args = parser.parse_args()

    status = get_status()

    if args.action == 'check':
        print("Status: " + status)
    elif args.action == 'restart' and status != 'active':
        force_restart()

if __name__ == "__main__":
    main()